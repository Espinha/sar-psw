#  Adapted from: https://github.com/vinodpandey/python-port-forward/blob/master/port-forward.py
#  Author: Mario Scondo (www.Linux-Support.com)
# Date: 2010-01-08
# Script template by Stephen Chappell

import socket
from threading import Thread
import time

import os

def start_new_server(listeningPort, fwdHostname,fwdPort):
    Thread(target=server,args=(listeningPort, fwdHostname, fwdPort)).start()

def start_all_connections():
    pass

def server(listeningPort=8080, fwdHostname='', fwdPort=8090):
    try:
        dock_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        dock_socket.bind(('', listeningPort))
        dock_socket.listen(5)
        while True:
            client_socket = dock_socket.accept()[0]
            server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_socket.connect((fwdHostname, fwdPort))

            Thread(target=forward, args=(client_socket, server_socket)).start()
            Thread(target=forward, args=(server_socket, client_socket)).start()
    finally:
        Thread(target=server, args=(listeningPort, '', fwdPort)).start()

def forward(source, destination):
    string = ' '
    while string:
        string = source.recv(1024)
        if string:
            destination.sendall(string)
        else:
            source.shutdown(socket.SHUT_RD)
            destination.shutdown(socket.SHUT_WR)
import random
import socket

from django.shortcuts import render
from .models import Clients, Connections 
from django.db.models import Max 
from .DiffieHellman import *
from .api import send_mail, addEntry 
from .conn_handler import start_new_server

# Create your views here.  

def index(request):
    return render(request, 'interface/index.html')

def add_client(request):
    if request.method == 'POST':
        c = Clients(ip = request.POST['ip'], email = request.POST['email'])
        c.save()
        context = { 'message' : 'Client successfully added.'}
        return render(request, 'interface/success.html', context)

    else:
        return render(request, 'interface/add_client.html')

def init_connection(request):
    if request.method == 'POST':
        request.session['client_ip'] = request.POST['client']

        gr = random.choice([5,14,15,16,17])
        dh = DiffieHellman(group = gr)
        pubkey = dh.publicKey

        try:
        	spi = Connections.objects.aggregate(Max('spi'))['spi__max'] + 2
        except:
        	spi = 0
        request.session['spi'] = spi

        request.session['dh'] = dh 
        request.session['dstemail'] = request.POST['dst']

        sender = Clients.objects.get(ip = request.POST['client']) 

        port = random.randint(49152, 49162)
        unique = False
        while not unique:
            try:
                Connections.objects.get(outboundport = port)
                port = random.randint(49152, 49162)
            except Connections.DoesNotExist:
                # unique port, pass
                pass
            try:
                Connections.objects.get(clientport = port)
                port = random.randint(49152, 49162)
            except Connections.DoesNotExist:
                unique = True
                pass

        address = request.POST['src'] + ':' + str(port) 
        request.session['srcaddr'] = address

        mail = send_mail(sender.email, request.POST['pw'], request.POST['dst'], 'Connect to me', address, pubkey, spi, gr) 

        if mail == 'FAILED':
            clients = Clients.objects.all()
            context = { 'message' : 'Failed to send email. Please try again.' , 'client_list' : clients }
            return render(request, 'interface/init_connection.html', context)
        else:
            return render(request, 'interface/finish_init.html')

    else:
        clients = Clients.objects.all()
        context = { 'client_list' : clients }
        return render(request, 'interface/init_connection.html', context)

def finish_init(request):
    client = Clients.objects.get(ip = request.session['client_ip']) 
    dh = request.session['dh']
    dh.genKey(int(request.POST['code']))
    sa_key = hexlify(dh.getKey())
    clientport = random.randint(49152, 49162)
    outport = random.randint(49152, 49162)
    unique = False
    while not unique:
        if not Connections.objects.filter(clientport__in = [clientport, outport]):
            pass
        else:
            clientport = random.randint(49152, 49162)
        if not Connections.objects.filter(outboundport__in = [clientport, outport]):
            unique = True
        else:
            outport = random.randint(49152, 49162)

    dst_addr = request.POST['ip'].split(':')

    con = Connections(clientid = client, clientport = clientport, outboundport = outport, dstbrokerip = dst_addr[0], dstbrokerport = dst_addr[1], sa_key = sa_key, dstemail = request.session['dstemail'], spi = request.session['spi'])
    con.save()

    src_addr = request.session['srcaddr'].split(':')

    addEntry(src_addr[0], src_addr[0] + '/32[' + src_addr[1] + ']', dst_addr[0], dst_addr[0] + '/32[' + str(dst_addr[1]) + ']', con.spi, '0x' + str(sa_key.decode('utf-8')))

    start_new_server(49151, dst_addr[0], dst_addr[1])
    start_new_server(outport + 1, '', clientport)

    request.session.clear()

    context = { 'message' : 'Connection successfully added.' }
    return render(request, 'interface/success.html', context)

def accept_connection(request):
    if request.method == 'POST':
        client = Clients.objects.get(ip = request.POST['client_ip']) 
        dh = DiffieHellman(group = int(request.POST['group']))
        pubkey = dh.publicKey
        dh.genKey(int(request.POST['code']))
        sa_key = hexlify(dh.getKey())
        clientport = random.randint(49152, 49162)
        outport = random.randint(49152, 49162)
        unique = False
        while not unique:
            if not Connections.objects.filter(clientport__in = [clientport, outport]):
                pass
            else:
                clientport = random.randint(49152, 49162)
            if not Connections.objects.filter(outboundport__in = [clientport, outport]):
                unique = True
            else:
                outport = random.randint(49152, 49162)

        dst_addr = request.POST['ip'].split(':')

        src_addr = request.POST['src'] + ':' + str(outport)

        mail = send_mail(client.email, request.POST['pw'], request.POST['dst'], 'My ip address', src_addr, pubkey) 

        spi = request.POST['spi']

        if mail == 'FAILED':
            clients = Clients.objects.all()
            context = { 'message' : 'Failed to send email. Please try again.' , 'client_list' : clients, 'groups' : [5,14,15,16,17,18] }
            return render(request, 'interface/recv_connection.html', context)
        else:
            con = Connections(clientid = client, clientport = clientport, outboundport = outport, dstbrokerip = dst_addr[0], dstbrokerport = dst_addr[1], sa_key = sa_key, dstemail = request.POST['dst'], spi = spi)
            con.save()

            addEntry(request.POST['src'], request.POST['src'] + '/32[' + str(outport) + ']', dst_addr[0], dst_addr[0] + '/32[' + str(dst_addr[1]) + ']', con.spi, '0x' + str(sa_key.decode('utf-8')), accept_connection=True)

            start_new_server(49151, dst_addr[0], dst_addr[1])
            start_new_server(outport + 1, '', clientport)

            context = { 'message' : 'Connection successfully added.' }
            return render(request, 'interface/success.html', context)

    else:
        clients = Clients.objects.all()
        context = { 'client_list' : clients , 'groups' : [5,14,15,16,17,18] }
        return render(request, 'interface/recv_connection.html', context)

def close_con(request):
    if request.method == 'POST':
        Connections.objects.get(id = request.POST['conid']).delete()
        context = { 'message' : 'Connection successfully deleted.' }
        return render(request, 'interface/success.html', context)
    else:
        conns = Connections.objects.all()
        context = { 'conn_list' : conns }
        return render(request, 'interface/close_con.html', context)

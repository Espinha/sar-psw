from __future__ import unicode_literals

from django.db import models

class Clients(models.Model):
    ip = models.TextField()
    email = models.TextField()

    class Meta:
        managed = False
        db_table = 'clients'

class Connections(models.Model):
    clientid = models.ForeignKey(Clients, models.DO_NOTHING, db_column='clientid')
    clientport = models.IntegerField(unique=True)
    outboundport = models.IntegerField(unique=True)
    dstbrokerip = models.TextField()
    dstbrokerport = models.IntegerField()
    sa_key = models.TextField(unique=True)
    dstemail = models.TextField()
    spi = models.IntegerField(unique=True, null=True)
    service = models.TextField(null=True)

    class Meta:
        managed = False
        db_table = 'connections'

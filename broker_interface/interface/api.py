import subprocess
import getpass
import os
import stat

# Rendez-vous
import smtplib

def runAsSudo(command):
    global p
    if p == None or p == "":
        print('Error with password')
        return None
    os.system('echo %s|sudo -S %s' % (str(p), command))

def addEntry(srcIP, srcPort, dstIP, dstPort, spi, key, accept_connection=False):
    spi = int(spi)
    if accept_connection == False:
        second_spi = spi+1
    else:
        second_spi = spi
        spi = spi+1
    
    filename = os.getcwd() + '/script.sh'
    f = open(filename, 'w+')
    """sudo setkey -c <<EOF
    > spdadd 10.0.1.1/32[20] 10.0.2.1/32[21] any -P out ipsec
    > esp/transport/10.0.1.1-10.0.2.1/require ;
    > spdadd 10.0.2.1/32[21] 10.0.1.1/32[20] any -P in ipsec
    > esp/transport/10.0.2.1-10.0.1.1/require ;
    > add 10.0.1.1 10.0.2.1 esp 0x1001 -m transport -E blowfish-cbc "0x93929292929929239939393939" ;
    > add 10.0.2.1 10.0.1.1 esp 0x1002 -m transport -E blowfish-cbc "0x93929292929929239939393939" ;
    > EOF"""

    f.write('setkey -c <<EOF\nspdadd ' + srcPort + ' ' + dstPort+' any -P out ipsec\nesp/transport/' +srcIP+'-'+dstIP+'/require ;\nspdadd '+dstPort+' '+srcPort+' any -P out ipsec\nesp/transport/'+dstIP+'-'+srcIP+'/require ;\nadd '+ srcIP + ' ' + dstIP + ' esp ' + str(spi) + ' -m transport\n-E blowfish-cbc ' + key + ';\nadd ' + dstIP + ' ' + srcIP + ' esp ' + str(second_spi) + ' -m transport\n-E blowfish-cbc ' + key + ';\nEOF')

    f.close()
    os.chmod(filename, stat.S_IRWXU)
    subprocess.call(os.getcwd() + '/script.sh', shell=True)
    os.unlink(filename)

def delEntry(src, dst, spi):
    filename = os.getcwd() + '/script.sh'
    f = open(filename, 'wr')
    f.write('setkey -c <<EOF\ndeleteAll ' + src + ' ' + dst + ' esp\nEOF')
    f.close()
    os.chmod(filename, stat.S_IRWXU)
    subprocess.call(os.getcwd() + '/script.sh', shell=True)
    os.unlink(filename)

def send_mail(sender, pw, receiver, subject, myIP, publicKey, spi=None, group=None):
    receiver = [receiver]

    # Prepare actual message
    # JSON???
    if group:
        message = "---- CODE -----\n" + str(publicKey) + '\n---- SPI ----\n' + str(spi) + '\n\n--- Group ---\n' + str(group) + '\n\n--- My IP ----\n' + myIP 
    else:
        message = "---- CODE -----\n" + str(publicKey) + '\n---- SPI ----\n' + str(spi) + '\n\n--- My IP ----\n' + myIP 
    message = 'From: %s\nTo: %s\nSubject: %s\n\n%s' % (sender, ', '.join(receiver), subject, message)

    try:
        server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        #server.starttls()
        server.ehlo()
        password = pw 
        server.login(sender, password)
        server.sendmail(sender, receiver, message)
        server.close()
        print('successfully sent the mail')
        return 'SUCCESS'
    except Exception as e:
        print(e)
        return 'FAILED'


#Testing purposes
"""
import random
prime_key = random.choice([5,14,15,16,17,18])

a = DiffieHellman(group=prime_key)
b = DiffieHellman(group=prime_key)
a.genKey(b.publicKey)
print hexlify(a.getKey())
addEntry('10.0.1.15', '10.0.1.15/32[20]','10.0.1.16', '10.0.1.16/32[21]', 5000, '0x' + hexlify(a.getKey()))
#delEntry('10.0.1.15', '10.0.1.16',5000"""

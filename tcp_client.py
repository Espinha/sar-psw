import socket

def send_packet(ip, port, message, buffersize=1024):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((ip, port))
	s.send(message)
	response = s.recv(buffersize)
	s.close()

	return response

print(send_packet('192.168.43.165', 49157, b'HELLO\n'))

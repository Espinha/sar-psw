import sqlite3

broker = sqlite3.connect('broker.db')

# File TABLE

cursor = broker.cursor()

query = "create table clients(" \
        "id integer primary key autoincrement, " \
        "ip text, " \
        "email text)"
cursor.execute(query)

query = "create table connections(" \
        "id integer primary key autoincrement, " \
        "clientid integer references clients(id), " \
        "clientport integer unique,"\
        "outboundport integer unique,"\
        "dstbrokerip text,"\
        "dstbrokerport integer,"\
        "sa_key text unique,"\
        "dstemail text," \
        "spi integer unique,"\
        "service text)"
cursor.execute(query)

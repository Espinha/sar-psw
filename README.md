# PSW - Personal Secure Wormholes #

The goal is to create secure subnets in which direct interactions between applications running in the personal networks of the end-users are encapsulated. The project consists in a Broker which is deployable as a service on any computer and forwards in/out traffic from a given service between a personal network and the Internet. Secure subnets are obtained via IPSec.

The project was developed in the Advanced Networks Security course of the Integrated Masters in Computers and Telematics Engineering (University of Aveiro).

## Rendez-vous point ##

Email exchange is used via Python's smtplib in order to exchange public keys and brokers' addresses/ports. Public keys are used to negotiate a shared secret via Diffie-Hellman, in order to define SA rules in each broker.

## Broker: ##
* Handles the Security Association Database and Security Policy Database to enable secure communication between end-to-end clients on different networks. Key exchange is done manually instead of using racoon/IKE.
* Maps all client addresses and ports within a given service with a contact port which is used to communicate with an external Broker, thus creating a mini-NAT.

### Web inteface ###

This interface enables configurationThis project uses Django to implement a configuration panel whose data is all manipulated in Python. The web interface allows a client in a personal network to set up a rendez-vous point in order to establish connection with another client and manage all created connections.

### Port Forwarder ###

A multithreaded port forwarder is implemented to receive all inbound traffic in one port and send traffic in other port. A port is allocated for the personal network and two other ports are allocated to each connection that is successfully established with another personal network. As it was previously said, the ports are mapped in order to be directly associated with a specific connection. By doing so, we can forward traffic to a client by checking its relation to a listening port (and vice-versa for client-external networks forwarding).

## Security ###

As previously told, secure subnets are implemented via IPSec. By creating Security Associations and Security Policies for encapsulated inbound/outbound traffic (ESP; transport mode) between two brokers communicating in specific mapped ports confidentiality and integrity is assured in all sent/received packets in TCP communications.

## The team ##

The entire solution was developed by me ([Espinha](https://bitbucket.org/Espinha)) and Luis Félix ([lcsfelix](https://bitbucket.org/lcsfelix)).